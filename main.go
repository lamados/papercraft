package main

import (
	"bytes"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/mmcdole/gofeed"
	"github.com/spf13/pflag"
)

var feedURLs []string
var updateTime time.Duration
var webhookURL string

func init() {
	rand.Seed(time.Now().UnixNano())

	pflag.StringSliceVarP(&feedURLs, "feed-urls", "f", nil, "feed URLs to watch for updates")
	pflag.DurationVarP(&updateTime, "update-time", "u", time.Minute, "amount of time between feed updates")
	pflag.StringVarP(&webhookURL, "webhook-url", "w", "", "Discord webhook URL to send notifications to")

	pflag.Parse()
}

func main() {
	if len(feedURLs) == 0 {
		log.Fatalln("no --feed-urls defined")
	}

	if webhookURL == "" {
		log.Fatalln("no --webhook-url defined")
	}

	log.Printf("setting up %d feed watchers", len(feedURLs))

	fp := gofeed.NewParser()
	for _, feedURL := range feedURLs {
		go func(feedURL string) {
			u, err := url.Parse(feedURL)
			if err != nil {
				log.Printf("failed to parse %s as an url: %v", feedURL, err)
			}

			// set feed name (more concise logs).
			feedName := u.Hostname()

			// create http client.
			client := http.Client{Timeout: 15 * time.Second}
			lastUpdate := time.Now()
			first := true

			for {
				if first {
					// wait for random duration on first run.
					delay := time.Duration(rand.Int63n(updateTime.Nanoseconds()))
					log.Printf("waiting %.1fs to update %s", delay.Seconds(), feedName)
					time.Sleep(delay)
					first = false
				} else {
					// wait for update duration.
					time.Sleep(updateTime)
				}

				// update feed.
				log.Printf("updating feed %s", feedName)
				feed, err := fp.ParseURL(feedURL)
				if err != nil {
					log.Printf("failed to parse feed %s: %v", feedName, err)
					continue
				}

				log.Printf("updated feed %s", feedName)

				// get most recent item.
				if len(feed.Items) == 0 {
					log.Printf("no items in feed")
					continue
				}

				item := feed.Items[0]
				title := item.Title
				authors := joinAuthorNames(item.Authors)

				// do processing if new item.
				if item.PublishedParsed.After(lastUpdate) {
					lastUpdate = *item.PublishedParsed

					log.Printf("new post in feed %s: '%s' by %s (%s)", feedName, title, authors, item.Link)

					// create message body.
					body := map[string]any{
						"content": "new papercraft <@114995048860483589>\n" +
							"'" + title + "' by " + authors + "\n" +
							item.Link,
					}

					raw, err := json.Marshal(&body)
					if err != nil {
						log.Printf("failed to encode body for %s: %v", feedName, err)
						continue
					}

					// create webhook request and post to Discord.
					req, err := http.NewRequest(http.MethodPost, webhookURL, bytes.NewReader(raw))
					if err != nil {
						log.Printf("failed to create request for %s: %v", feedName, err)
						continue
					}
					req.Header.Set("Content-Type", "application/json")

					resp, err := client.Do(req)
					if err != nil {
						log.Printf("failed to perform request for %s: %v", feedName, err)
						continue
					}

					log.Printf("pushed message for %s: %s", feedName, resp.Status)

					err = resp.Body.Close()
					if err != nil {
						log.Printf("failed to close body for %s: %v", feedName, err)
						continue
					}
				}
			}
		}(feedURL)
	}

	sc := make(chan os.Signal)
	signal.Notify(sc, os.Interrupt)
	<-sc
}

func joinAuthorNames(authors []*gofeed.Person) string {
	names := make([]string, 0, len(authors))
	for _, author := range authors {
		names = append(names, author.Name)
	}
	return strings.Join(names, ", ")
}
